Computational Strategies
========================

This lecture describes the concepts and strategies for reproducibility
in Bioinformatics.

* :download:`pdf slides <computational_environment_management.pdf>`
* :download:`pptx slides <computational_environment_management.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vShkoUtSv3pKEK7TnJnw6LARP7yriWz9TTYnL6mjWNyvnyVLdRc_hriGM6LmiWT-_F-OvRb91qYoiC6/pub?start=false&loop=false&delayms=60000

Lectures
--------

Intro
+++++

Runtime: 4:45

.. youtube:: qoGo1Gw4JTI

miniconda
+++++++++

Runtime: 3:02

.. youtube:: qGf6U3iXcTA

miniconda demo
++++++++++++++

Runtime: 11:07

.. youtube:: k_iZ6FVCAko

pip
+++

Runtime: 1:53

.. youtube:: a1fqcKvRxxs

pip demo
++++++++

Runtime: 4:06

.. youtube:: WkJ5orLBekI

R packages
++++++++++

Runtime: 1:17

.. youtube:: 4_oWR6nyZLY

R packages demo
+++++++++++++++

Runtime: 8:04

.. youtube:: LL6Jj3a1Fe8

git
+++

Runtime: 2:30

.. youtube:: oiJkQZy4r7c

git demo
++++++++

Runtime: 9:05

.. youtube:: psPE7RItKj8

github/bitbucket
++++++++++++++++

Runtime: 13:22

.. youtube:: V0CBNLxGtTY

Summary and tail matter
+++++++++++++++++++++++

Runtime: 2:31

.. youtube:: UyQTm2rzdic
