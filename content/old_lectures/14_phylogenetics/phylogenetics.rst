Phylogenetics
=============

This lecture describes phylogenetics concepts and methods.

* :download:`pdf slides <13_phylogenetics.pdf>`
* :download:`pptx slides <13_phylogenetics.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTAMIOYudlmgpeRKfLf68AD6_Edi6J4crgVTzKvoucsOZ4LvaJG6H5tZVpHy4WRVEtSgkwpV1XbHY65/pub?start=false&loop=false&delayms=60000

Lectures
--------

The lecture segments are available below and at this playlist:

`BF528 - Lecture 12 ChIP-Seq <https://www.youtube.com/playlist?list=PLpm6hAgFrR770EjFhGhY_M5H1DAzRIrhm>`_

Introduction
++++++++++++

Playtime: 9:05

.. youtube:: e_QmqV6iWas

Molecular Phylogenetics
+++++++++++++++++++++++

Playtime: 3:47

.. youtube:: LxtpLHhBU9Y

Terminology
+++++++++++

Playtime: 4:40

.. youtube:: fCQYYtP23sc

Multiple Sequence Alignment
+++++++++++++++++++++++++++

Playtime: 3:29

.. youtube:: VVRj4M9943I

Distance Based Tree Reconstruction Methods
++++++++++++++++++++++++++++++++++++++++++

Playtime: 4:41

.. youtube:: UHmW52yACFA

WGPMA, Neighbor Joining
+++++++++++++++++++++++

Playtime: 6:37

.. youtube:: z42UKQDvp_8

Character Based Tree Reconstruction Methods
+++++++++++++++++++++++++++++++++++++++++++

Playtime: 7:52

.. youtube:: qhQalaYI4fs

Tree Assessment
+++++++++++++++

Playtime: 8:08

.. youtube:: oeD8NqCqHd4
