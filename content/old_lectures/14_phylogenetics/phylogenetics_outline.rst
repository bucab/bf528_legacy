Phylogenetics Outline
=====================

#. Overview
	#. A truly foundational topic
		#. History of phylogeny is rooted in taxonomy
			#. We've always wanted to group together organisms using common traits
		#. Check out bioinformatics textbooks released before NGS
			#. Phylogeny is sure to have its own chapter
	#. Phylogeny as a tool
		#. Subject of research in the 1980s and 1990s when DNA sequences became more available
		#. Renewed interest now with the advent of NGS and the explosion of sequence data
		#. Main goal is to examine and visualize relationships between species/genes
		#. Can be applied to many questions and problems
			#. Translational and epidemiological
				#. Origin and spread of a viral infection
				#. Genealogical relationship of somatic cells during differentiation or cancer development
			#. Other more basic quesitons
				#. Origin and relationship between paralogs of a gene
				#. Migration patterns of species
				#. Evolution of language
				#. Annotation of newly sequenced genomes
					#. Classification of metagenomic sequences
					#. Identification of genes, regulatory elements based on homology to known elements
				#. Reconstruction of ancestral genomes
	#. Goal of this lecture/material
		#. Discuss phylogenetics (molecular phylogenetics in particular)
		#. Understand some of the major methods of phylogenetic analysis
			#. Parsimony
			#. Distance
			#. Likelihood
			#. Bayesian methods
		#. Look at some of the popular phylogenetics packages
			#. Understand when/why to use each
			#. Understand how to compare results across them
#. Basic phylogenetic tree concepts
	#. The phylogenetic tree as a model
	#. Important tree terminology
	#. Rooted tree example
	#. Unrooted tree example
	#. Reconstruction of phylogenetic trees
		#. Methods can be distilled down to either
			#. Distance-based
				#. A "distance" is calculated between pairs of sequences
				#. Resulting distance matrix of pairwise distances is used for reconstruction
			#. Character-based
				#. Simultaneous comparison of all sequences in a given alignment
				#. One character at a time within the alignment is used for tree reconstruction
				#. Each possible tree is assigned a score
				#. Collection of trees is searched for a maximum scoring "true" tree
#. Distance-based tree reconstruction
	#. Calculation of distance between two sequences
		#. Can be done using a Markov chain model of nucleotide substitution
		#. Different models have different parameters and assumptions
			#. JC69 model overview
			#. K80 model overview
			#. GTR model overview
	#. Distance matrix methods
		#. The multiple sequence alignment is no longer needed once distance matrix is obtained
		#. Overview of different distance-based methods
			#. Least squares
			#. Minimum evolution
			#. Neighbor joining
	#. Strengths and weaknesses of distance methods
		#. Advantages over character-based methods
			#. SPEED
		#. Disadvantages
			#. Performs poorly for very divergent sequences
			#. Sensitive to gaps in sequence alignment
#. Character-based tree reconstruction
	#. Maximum parsimony
		#. Parsimony and parsimony tree scores
		#. Strengths and weaknesses of parsimony
	#. Maximum likelihood
		#. Basis of maximum likelihood
		#. Maximum likelihood tree reconstruction
		#. Strengths and weaknesses of maximum likelihood
	#. Bayesian methods
		#. Basis of Bayesian inference
		#. Bayesian phylogenetics
		#. Strengths and weaknesses of Bayesian inference
#. Assessment of phylogenetic methods
	#. Consistency
	#. Efficiency
	#. Robustness
	#. Computational speed and feasibility
#. Some commonly used phylogenetic programs
	#. MEGA
	#. MrBayes
	#. PAUP
	#. PHYLIP
#. Outlook
	#. Phylogenomics
		#. Approaches to analyze large data sets
			#. Supertree
			#. Supermatrix