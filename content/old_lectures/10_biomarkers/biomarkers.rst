Biomarkers
==========

This lecture was delivered by guest lecturer Marc Lenburg.

Lecture
-------

The lecture segments are available below and at this playlist:

`BF528 - Lecture 11 Biomarkers <https://www.youtube.com/playlist?list=PLpm6hAgFrR752ebIE6LuDv-iZ7WR8TWWF>`_

Intro
+++++

Playtime: 3:38

.. youtube:: 6-w-v3YDH2c

Definitions
+++++++++++

Playtime: 3:48

.. youtube:: v60rLGipa3Y

Gene Expression Biomarkers
++++++++++++++++++++++++++

Playtime: 2:25

.. youtube:: dn94I3KGeSk

Challenges
++++++++++

Playtime: 3:57

.. youtube:: FLD7mUb3ph8

Biomarker Examples
++++++++++++++++++

Playtime: 4:13

.. youtube:: WsOBrufEeZs

Regulatory Concerns
+++++++++++++++++++

Playtime: 2:41

.. youtube:: T_k-ho3XFiU

MAQCII and Biomarker Validation
+++++++++++++++++++++++++++++++

Playtime: 7:35

.. youtube:: LZt1nbXnwQI

Biomarker Failures
++++++++++++++++++

Playtime: 5:44

.. youtube:: WVaSddiyp0U

Robust Biomarkers
+++++++++++++++++

Playtime: 3:34

.. youtube:: tZAGWmE8jZ8

Airway Biomarker 1
++++++++++++++++++

Playtime: 5:12

.. youtube:: 50yDBohnRg

Airway Biomarker 2
++++++++++++++++++

Playtime: 7:37

.. youtube:: xKRsUgf6_QA
