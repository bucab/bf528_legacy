Computational Skills Primer Outline
==================================================

#. Basic introduction to SCC and get started with SCC
	#. What is SCC and its applications ?
	#. SCC architecture , storage- projects, home, scratch space
	#. Get an account
#. Softwares required to connect with SCC 
	#. SSH client
	#. Connect with GUI (X forwarding)
	#. FTP/ SFTP for file transfer	
#. Working with SCC Part I : Navigating
	#. Filesystem Structure
	#. Data sets used in tutorial
	#. Working with Files/Directories (overview of online content)
	#. Creating files in vi, vi, gedit (overview of online material)
	#. Store your files
	#. Controlling access to your files
	#. Tar and compressed files
#. Working with SCC Part II- Environment and Batch jobs
	#. Customizing your Environment
	#. How to find software and tools installed on scc
	#. Submit a Batch Job
		#. Embedding scripts
		#. Software version and modules
		#. Defining parameters
		#. Resource Usage and Limit
		#. Interactive session (qsh login)
		#. Screen Command 
#. Remote VPN Access
#. Discussion
