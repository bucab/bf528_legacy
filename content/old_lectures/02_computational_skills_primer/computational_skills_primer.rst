Computational Skills Primer
===========================
This lecture is a quick overview of basic linux commands and usage of command
line to work with Shared Computing Cluster (SCC). We will have hands-on
session in class as we go through different concepts.

* :download:`pdf slides <Computational_Skills_Primer.pdf>`
* :download:`pptx slides <Computational_Skills_Primer.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQ02Bxui_M9XJUlrjL1GcGJvsPi5LVkJuxcli8s-gv9TKJzuSuqZCwy6MMc1YyxyFhwtPFsruMIkqCd/pub?start=false&loop=false&delayms=3000

Lectures
--------

This lecture is available below or at `this playlist`_.

.. _this playlist: https://www.youtube.com/playlist?list=PLpm6hAgFrR75Y4bgYlM_vznOJ-9lhKIK-

Introduction
++++++++++++

Playtime: 4:40

.. youtube:: X_z4acWoMU8

SCC Introduction
++++++++++++++++

Playtime: 7:22

.. youtube:: GcUPrlaFW8g

Assignment
----------

``terminal_quest``
++++++++++++++++++

Connect to SCC and follow the instructions of the README.md found there:

.. code:: bash

   $ cat /projectnb/bf528/README.md

Once you have done this, run the following commands to enable the access to the
``terminal_quest`` executable:

.. code:: bash

   $ source activate terminal_quest
   $ terminal_quest

Instructions will be printed to the screen. **This assignment will not be
collected or graded** - it is for your own edification (and enjoyment).

Advance CLI Workshop
++++++++++++++++++++

Familiarize yourself with the material on advanced command line usage found
here:

`Workshop 3. Advanced CLI and Tools <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/03_advanced_cli/03_advanced_cli.html>`_
