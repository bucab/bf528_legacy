Accuracy
========

Accurate writing conveys correct information, and is the baseline requirement of good writing. Accuracy relates primarily to the content of what is being described, though incorrect or awkward grammar can obscure otherwise accurate writing. Accuracy involves producing the correct structure and components of a requested body of writing, be it a class assignment with specified plots and results, or a journal article that must have specific sections in a predetermined order.

Common mistakes
+++++++++++++++

- Omitting key components, e.g. sections, plots, tables, etc
- Including incorrect information, or omitting requested information, from plots and tables
- Incomplete description of methods or results
- Poor or awkward grammar that interferes with the reader�s comprehension of the content (also see Clarity of language and Thoroughness)

