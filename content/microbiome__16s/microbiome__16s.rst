Microbiome: 16S
===============

This lecture introduces the concept of the microbiome and how we use the
ribosomal subunit 16S to characterize a population of microbes.

* :download:`pdf slides <microbiome_16S.pdf>`
* :download:`pptx slides <microbiome_16S.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQt0DvUo-HvVBKEowigxFLoAQzh1-zaULnUTntOxVLIUVXzdjWlx-187v7JRshgDnPCNRlkFZ7XIhXx/pub?start=false&loop=false&delayms=60000

Lectures
--------

The same video lecture is for both Microbiome 16S and Metagenomic
lectures

`Microbiome`_

.. _Microbiome: https://mymedia.bu.edu/media/BF528+-+Microbiome+%2816S+%26+Metagenomic+Sequencing%29/1_qgd48kve
