Computational Pipeline Strategies
=================================

This lecture describes the workflow management software concept and
demonstrates two tools commonly used in Bioinformatics.

* :download:`pdf slides <computational_pipeline_strategies.pdf>`
* :download:`pptx slides <computational_pipeline_strategies.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQ_mAwA9Lc43MC9ZeT0vTkc4zuAyIObEybtGh7DE8FOeIpocD9KNtbVuEtK36DCiEw4-2wlR-vZtndr/pub?start=false&loop=false&delayms=60000

Lectures
--------

Intro
+++++

Runtime: 6:51

.. youtube:: 3XEv8i8HZ9M

Workflow Properties
+++++++++++++++++++

Runtime: 5:44

.. youtube:: MKMTHtmNZpw

Nextflow
++++++++

Runtime: 11:48

.. youtube:: mKdkaxV6NC0

Nextflow demo
+++++++++++++

Runtime: 13:27

.. youtube:: IPogF8djO30

snakemake
+++++++++

Runtime: 13:19

.. youtube:: VSUkzn4hunw
