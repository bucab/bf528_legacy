Replicability vs Reproducibility Strategies
===========================================

This lecture describes the concepts and strategies for reproducibility
in Bioinformatics.

* :download:`pdf slides <reproducibility.pdf>`
* :download:`pptx slides <reproducibility.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vSoklMcPz61vb1qmozSyJfk1KAcmZX_rZvAiRc-PdiCk1h8vrssgMY9ArH0MxH83K_ph7mmX2aomsT2/pub?start=false&loop=false&delayms=60000

Lectures
--------

Crisis
++++++

Runtime: 10:10

.. youtube:: ewhIGIST1Qg

Definitions
+++++++++++

Runtime: 3:50

.. youtube:: 7SAp7vzP6RU

Analysis Components
+++++++++++++++++++

Runtime: 2:05

.. youtube:: -CO7xOl8wfk

Computational Environment
+++++++++++++++++++++++++

Runtime: 6:33

.. youtube:: Iq36mjx9jvQ

Computational Environment Demo
++++++++++++++++++++++++++++++

Runtime: 10:04

.. youtube:: SdjhNbdVsJ0

Data
++++

Runtime: 7:02

.. youtube:: 0gRCGTT1H_M

Code
++++

Runtime: 10:41

.. youtube:: R_6H18sclWA

Presentation
++++++++++++

Runtime: 10:35

.. youtube:: ekzPjWGB7z4

