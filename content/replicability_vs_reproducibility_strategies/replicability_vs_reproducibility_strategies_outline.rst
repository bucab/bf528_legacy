Replicability vs Reproducibility Strategies Outline
===================================================

1. Overview

  A. What is reproducibility?

     #. Reproduce: create identical conditions to a previous result and come
        to the identical result
     #. For us: apply the same methodology (ideally, the same code) to the same
        data and obtain the same result
     #. If the code is unavailable, we must first *reimplement* the method, and
        then we may attempt to *reproduce* the result

  #. What is replicability?

     #. Replicate: to repeat a previously described experiment to material or
        data
     #. For us: apply the same or different methodology that seeks to answer
        the same question (e.g. RNASeq analysis with STAR+featurecount+DESeq2
        vs. kallisto) on the same or different data
     #. If the code is unavailable, we must first *reimplement* the method, and
        then we may attempt to *replicate* the result

  #. In consistency in definition

     #. Depending on who you ask, reproducibility and replicability have
        opposite definitions: http://languagelog.ldc.upenn.edu/nll/?p=21956
     #. For our purposes:
        *reproduce* means show that the methodology was applied correctly
        *replicate* means show the scientific result is "true"
        *reimplement* means turn the verbal description of a methodology into
        new reification of the methodology
        *reapply* means run existing code on existing or new data

     #. In summary:
        Reproducibility and replicability are about *results*
        Reimplementation and reapplication are about *methods (code)*

#. Reproducibility in Bioinformatics

   #. Main components of an analysis

      #. Computational Environment
      #. Data
      #. Code
      #. Presentation

   #. Kitchen Analogy

      #. Computational Environment == kitchen + tools
      #. Data == raw ingredients (e.g. whole potatoes, flour)
      #. Code == stirring, whisking, frying, baking
      #. Presentation == plating, garnish

#. Computational Environment

   #. Software Dependencies

      #. Software packages require other software packages
      #. e.g. DESeq2 -> bioconductor -> R -> OS libraries
      #. Every piece of software has a *version*, and some versions don't work
         together
      #. Not all OSes have all software versions, e.g. linux vs MacOS vs
         Windows
      #. Must describe the tools *and* versions of those tools *and* their
         dependencies on the specific OS to fully recreate an computational
         environment

   #. Hardware Environment

      #. Analyses are run on specific hardware, e.g. Intel, Macbook, shared
         compute cluster, GPU
      #. Some software is specific to hardware, e.g. qsub, CUDA
      #. Any known hardware specificity must be described to recreate a
         computational environment

   #. Environment Management Tools

      #. modules

         #. package management software installed locally
         #. load specific versions of packages into your current environment
         #. dependencies between packages specified explicitly
         #. Strengths: software is maintained locally
         #. Disadvantages: manually maintained, requires either system
            administrator support or substantial system administration
            knowledge

      #. miniconda

         #. software environment management software
         #. create an "environment" for each analysis that is described by a
            set of software packages with their versions
         #. software packages are managed in *channels* built for specific
            versions and hardware
         #. Strengths: easy to install, thorough description of computational
            environment
         #. Disadvantages: packages must be in existing channels (unless you
            learn to build your own)

      #. Containerization

         #. Containers are like mini operatings systems
         #. Contain all specific versions of software needed to run an analysis
         #. Custom code can be loaded into the container so it can be run as a
            single unit
         #. Current technologies:

            #. docker
            #. singularity

         #. Strengths: ideal strategy for encapsulating and rerunning the exact
            analysis in other computational environments
         #. Disadvantages: requires system administration knowhow, some
            containerization systems have prohibitive technical requirements
            in shared compute cluster environments

#. Source, Support, and Transformed Data

   #. Source data

      #. Raw, unprocessed data is always preferred, e.g. untrimmed reads taken
         directly from the Illumina software pipeline
      #. Data should be deposited in a publicly available repository, e.g. Gene
         Expression Omnibus, dbGaP, figshare, etc.
      #. Repository should have established plan for longevity, i.e. don't just
         throw your data on a random FTP server that might not be supported in
         the future

   #. Support data

      #. Data used to condense, process, and annotate source data, e.g. human
         genome sequence, bowtie index, GENCODE annotations, GO annotations
      #. If support data is
      
            #. Available from a static and supported location, e.g. GENCODE, ok
               to link your code to a URL
            #. That static URL points to data that doesn't change, i.e. not a
               database query to a database that is modified over time

      #. If not, download a copy of the data and include with your workflow

   #. Transformed data

      #. Data derived from source data, e.g. quality trimmed sequence data,
         normalized counts matrices, differential expression statistics
      #. Derived data is (usually) what we use to interpret our results
      #. Your code should be the *recipe* for creating transformed data, thus
         transformed data should generally not be included in your workflow,
         EXCEPT:
      #. The final transformed form of the data that was used to interpret the
         results

#. Code

   #. Code is a recipe for transforming uninterpretable, source data into a
      processed, interpretable form
   #. Usually a combination of analysis code and glue code that connects tools
      and other analysis code
   #. Code will undergo tremendous iteration and change from its first writing
      to the final version
   #. We are tempted to create copies of scripts to 'save' previous work, which
      over time will amount to messy, incomprehensible, unaccountable code that
      you yourself won't remember how to run (nevermind someone else in three
      years who emails your boss)
   #. Version control systems, e.g. git, are superior to track changes in code
   #. Web platforms like github.com and bitbucket.org host code that enables:

      #. seamless backup of code
      #. simplifies sharing and collaboration
      #. public disemination of analysis code upon publication

   #. Code should be well documented, both in comments and README files

#. Presentation

   #. Tables and figures are the main vehicles of scientific communication
   #. Tables

      #. Avoid copy paste when creating data tables whenever possible, humans
         make mistakes
      #. Tabular data should (almost) always be included in a data format
         e.g. CSV or excel as supplementary materials as well as in text
      #. Tabular data uploaded as supplementary materials should be as machine-
         and human- readable as possible (e.g. consistent, controlled textual
         column values, formatted with column headers and no extra comment
         rows, no irregular formatting, etc)

   #. Figures

      #. Create as much of the figure programmatically as possible (e.g.
         matplotlib)
      #. Output to Scalable Vector Format (SVG) rather than bitmap formats
         (PNG, JPG), then use Inkscape or Adobe Illustrator to export high
         resolution bitmaps
      #. Outputting to SVG allows easier manipulation of figure elements and
         styles, especially in multi-panel figures

