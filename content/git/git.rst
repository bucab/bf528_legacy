git
===

This lecture covers an overview of the source code version control software git.


* :download:`pdf slides <git.pdf>`
* :download:`pptx slides <git.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vRDq6AzJsjbm4O-e5sZmJCLbwLdsVJteD_Vym7xJJOJiZQJKoaD9ezI7u_sYG--TWSekHaV3wys_MLe/pub?start=false&loop=false&delayms=60000

Assignment
----------

Project work
