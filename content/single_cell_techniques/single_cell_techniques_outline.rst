Sequence Analysis 3 - Single Cell Techniques Outline
====================================================

#. Introduction

   #. Cells are important
   #. Each cell has 2x3.3B DNA bp and 600M mRNA bases
   #. Every context has multiple cell types/states

   #. Examples
   
      #. brain
      #. microbial ecology
      #. tumor

   #. So far, we have only seen the forest; we want to see the trees
   #. Questions:
   
      #. What different cell types are in a sample?
      #. What is the proportion of these cell types?
      #. How do the transcripts differ between them?
      #. Do certain cells respond in a specific way to stimulus?
      #. How do cells evolve/develop over time?
      #. What is the level of mosaicism in somatic tissues?

#. Single cell sequencing workflow

   #. Isolation of cells or nuclei
   #. Possible sorting of cells (FACS)
   #. Nucleic acid extraction and processing

      #. femtograms-picograms of starting material!
      #. RNA -> cDNA reverse transcription
      #. Unique Molecular Identifiers (UMI) for RNA
      #. PCR, in vitro transcription (RNA), or Multiple displacement
         amplification (DNA)

   #. Sequencing library preparation + sequencing

   #. Analysis
   
      #. Demultiplexing
      #. UMI Collapsing
      #. Alignment
      #. Quantification
      #. Clustering (PCA, tSNE)

#. Flavors

   #. DNA
   #. DNA methylome (bisulfite treatment)
   #. RNA
   #. Single nuclear

#. Challenges

   #. PCR bias
