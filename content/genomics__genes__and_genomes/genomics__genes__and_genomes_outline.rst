Genomics, Genes, and Genomes Outline
====================================

#. Overview

   #. Origin of "Genomics": 1987
   #. What is genomics?
   #. Central Dogma - DNA -> RNA
   #. What can genomics tell us?

      #. Starting with complete DNA sequence
      #. Gene sequence -> Protein sequence -> Protein/gene function
      #. Gene regulatory sequence -> gene expression
      #. DNA variation within population -> Disease -> protein

   #. How do we study genomics?

      #. Isolate nucleic acid molecules
      #. Determine nucleotide sequence using biochemistry
      #. Digitize nucleotide sequences
      #. Examine digital sequences to identify patterns
      #. Relate patterns to biological observations

   #. Sequencing techniques

      #. Sanger sequencing
      #. High-throughput (or pyro-) sequencing

#. Sequence analysis

   #. Broad analysis types

      #. Assembly
      #. Mapping
      #. Pattern recognition

   #. Sequence alignment

      #. Measure of relatedness
      #. Quantified by *sequence similarity*, or *% identity*
      #. Useful for any sequential moleculre type (DNA/RNA, Amino acids, protein
         secondary structure)
      #. High sequence similirity *probably* implies common history,
         function

   #. What alignments can tell us

      #. Homlogy (orthologs, paralogs)
      #. Genomic identity/origin of a sequence/individual
      #. Genome/gene structure (genic, RNA 2D, chromosome rearrangements)

   #. Example of aligned sequences
   #. Scoring/substitution matrices

      #. Used to quantify alignment quality
      #. Higher scores -> better alignment
      #. Examples

   #. Sequence alignment algorithms
   
      #. global - require beginning and end of both sequences to line up
      #. local - allow one sequence to align within a longer sequence
         without penalty
      #. global alignment example
      #. local alignment example
      #. multiplicity

         #. pairwise (2 sequences)
         #. muliple sequence alignment (3+ sequences)

      #. multiple sequence alignment example

#. Alignment Applications

   #. Genome assembly
   #. Exon microarray probes
   #. mRNA-Seq analysis
   #. DNA binding site discovery

#. Human genomics and gene expression

   #. Human genome project
   #. How was the human genome determined?

      #. International Human Genome Sequencing Consortium - Hierarchical
         shotgun sequencing (~10 years)
      #. Celera Technologies - shotgun sequencing (~3 years)

   #. Genome composition

      #. ~20k protein coding genes
      #. ~3% of sequence codes for protein-coding genes (Piovesan, A; Caracausi, M; Antonaros, F; Pelleri, MC; Vitale, L (2016). "GeneBase 1.1: a tool to summarize data from NCBI gene datasets and its application to an update of human gene statistics". Database: The Journal of Biological Databases and Curation. doi:10.1093/database/baw153. PMC 5199132.)
      #. Yet >85% of the genome is transcribed (Hangauer MJ, Vaughn IW, McManus MT (2013) Pervasive Transcription of the Human Genome Produces Thousands of Previously Unidentified Long Intergenic Noncoding RNAs. PLoS Genet 9(6): e1003569.)
      #. Repetitive elements may comprise >66% of genome (de Koning APJ, Gu W, Castoe TA, Batzer MA, Pollock DD (2011) Repetitive Elements May Comprise Over Two-Thirds of the Human Genome. PLoS Genet 7(12): e1002384. https://doi.org/10.1371/journal.pgen.1002384)

   #. The genome is all about genes

      #. Genic sequence
      #. What genes are different between humans?
      #. How are genes associated with disease?
      #. How are genes controlled?
      #. What do genes do?

   #. Gene expression definition
   #. What is a gene?

      #. A specific DNA sequence
      #. Fundamental unit of inheritance
      #. A molecule created by transcription of DNA into RNA, and then often
         translated into a protein
      #. "gene" is an abstract concept

   #. What is gene expression?

      #. Active mRNA transcription?
      #. mRNA abundance?
      #. mRNA translation?
      #. RNA function?
      #. Protein abundance?
      #. Protein function?
      #. YES

   #. The gene expression landscape

      #. mRNA - protein coding genes
      #. pre-mRNA - pre-spliced mRNA
      #. functional non-coding RNA (ncRNA)
      #. antisense
      #. pseudogenes

   #. How we measure gene expression

      #. mRNA transcription/translation: fluoresecent tagging + microscopy,
         ribosomal capture
      #. mRNA abundance: northern blot, qPCR, microarray, sequencing
      #. protein abundance: western blot, fluorescent tagging + microscopy, mass
         spectrometry, protein binding arrays
      #. mRNA/protein localization: fluorescent tagging + microscopy,
         immunohistochemistry

   #. mRNA measurements considerations

      #. most techniques measure *steady state RNA molecule abundance*
      #. measurements are *snapshots* of current cellular conditions
      #. poor concordance between mRNA and corresponding protein abundance
         measurements

