Genomics, Genes, and Genomes
============================

This lecture covers an overview of the human genome, the concepts of genes
and gene expression, and how we interrogate them.

* :download:`pdf slides <genetics_and_genomics.pdf>`
* :download:`pptx slides <genetics_and_genomics.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vRTcNda7r9JX_oLmuzNmpOPyUqLoQtullVFEj57KWQfbkl5X6XY6dCoHbGu2v3yuFWNp6msFeASLnQ1/pub?start=false&loop=false&delayms=60000

Lectures
--------

This lecture is available below or at `this playlist`_.

.. _this playlist: https://www.youtube.com/playlist?list=PLpm6hAgFrR75Y4bgYlM_vznOJ-9lhKIK-

Introduction
++++++++++++

Playtime: 10:08

.. youtube:: 4PAWxJxjp54

Sequencing Design Choices
+++++++++++++++++++++++++

Playtime: 4:49

.. youtube:: XAFGwc3KCb8

Read Mapping
++++++++++++

Playtime: 4:42

.. youtube:: deuz6V_Vdg8

Mapping Statistics
++++++++++++++++++

Playtime: 7:46

.. youtube:: JNhRJ3ZHKmM

fastq Format
++++++++++++

Playtime: 6:03

.. youtube:: jL7vTa_v19s

Sequencing Platform Comparison
++++++++++++++++++++++++++++++

Playtime: 6:08

.. youtube:: nKENYSMMvzE

Sequencing Databases
++++++++++++++++++++

Playtime: 4:12

.. youtube:: UnRmD_LCRZg


Read Simulation and Discussion
++++++++++++++++++++++++++++++

Playtime: 15:10

.. youtube:: eTeRdC16x2Y

Assignment
----------

Familiarize yourself with the material on bash scripting and cluster computing
found here:

`Workshop 6. Shell Scripts and Cluster Computing <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/06_cluster_computing/06_cluster_computing.html>`_
