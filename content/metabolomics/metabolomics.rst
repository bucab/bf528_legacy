Metabolomics
============

This lecture introduces the concept of metabolomics and briefly covers
metabolomic data, how it is generated, and how it is analyzed.

* :download:`pdf slides <metabolomics.pdf>`
* :download:`pptx slides <metabolomics.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQhRAdSzUxWjR1B224cMrqoSzG1mnPNonoqwYtjOX8Jg3rwr1VIbwc70Sejo3TKCTTkyircMZJox388/pub?start=false&loop=false&delayms=3000

Lectures
--------

`Metabolomics`_

.. _Metabolomics: https://mymedia.bu.edu/media/BF528+-+Metabolomics/1_55n00f9k
