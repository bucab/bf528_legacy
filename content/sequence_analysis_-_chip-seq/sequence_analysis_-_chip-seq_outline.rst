Sequence Analysis 4 - ChIP-Seq Outline
======================================

#. Overview
	#. What is ChIP-seq and why is it used?
	#. Protein binding in the larger context of gene regulation
	#. Experimental protocol in brief
	#. Comparison with RNA-seq
		#. Assay is depedent on antibodies and antibody quality
		#. Binding sites can be different between experiments
			#. Features are binding sites vs. gene annotation in RNA-seq
#. ChIP-seq analysis workflow
	#. Read-level and mapping QC
		#. Not all that different from RNA-seq
		#. Still working with mapping short reads
		#. Punctate transcription factor sites have characteristic distributions
			#. Strand cross-correlation can be used to gauge experiment quality
	#. Calling peaks on target vs. control
		#. Target protein and control are usually processed in parallel
			#. Control is either input genomic DNA or a mock IP experiment (IgG)
		#. Peaks are areas where binding is enriched in your ChIP experiment compared to control
			#. Your protein is binding somewhere within that enriched region
		#. Commonly used tools
			#. MACS, GEM, HOMER
	#. Motif analysis
		#. Sequence specificity of transcription factor binding
		#. Position Frequency Matrices (PFM) as a model
			#. Strengths and weaknesses
		#. De novo motif building
			#. Sampling-based methods (Gibbs)
			#. K-mer enrichment-based approaches
		#. Motif scanning as a classifier
		#. Motif enrichment analysis
			#. Leverages previous experiments
#. Functional characterization of protein binding
	#. Differential binding
		#. Goal is to find differences in protein binding across time or other conditions
		#. Similar in practice to using RNA-seq to quantify differential gene expression
			#. Packages used have same underlying models, assumptions
			#. Instead of providing gene annotation you either
				#. Provide a list of features (coordinates of genomic loci you're interested in)
				#. Let the software discover features of interest based on your input data
	#. Integration with gene expression
		#. End goal of assaying genome-wide binding of a protein is often to place it within context of gene regulation
		#. Nearest gene is often used as a proxy to determine targets
			#. Less than optimal given recent findings (Hi-C and TAD based approaches)
#. Variations on ChIP-seq
	#. Histone modifications
		#. Chemical modifications of histone tails can be assayed using ChIP-seq
		#. Different combinations of histone modifications correlate with different regulatory element types
			#. Becoming useful for genomic annotation of regulatory elements
		#. More difficult to analyze in practice than transcription factors
			#. Histone modifications are not sequence-specific
			#. Some modifications are more punctate and look like wider transcription factor binding sites
			#. Some modifications over a broader region so default peak calling parameters often cannot be used
				#. Analysis suites often have a "histone" setting or set of parameters
	#. ChIP-chip
		#. ChIP-chip is to ChIP-seq what microarray is to RNA-seq
		#. An older technology that you may run into
	#. ChIP-exo
		#. ChIP-seq with the additional step of using an exonuclease to trim bound sites
#. Other ways to measure protein binding
	#. In Vitro
		#. protein binding microarrays and UNIPROBE
		#. HT-SELEX