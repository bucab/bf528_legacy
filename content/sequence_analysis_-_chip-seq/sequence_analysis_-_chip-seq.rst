Sequence Analysis 4 - ChIP-Seq
==============================

This lecture describes ChIP-Seq experiments, methodology, and analysis.

* :download:`pdf slides <chip-seq.pdf>`
* :download:`pptx slides <chip-seq.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTn4TYtzS6V5Otdwk21kkEcpJYJT85QSKI9WyV02jg4eocvZ0DIoLCOO4Hc-JXG6e26myuMmVeeCqd4/pub?start=false&loop=false&delayms=60000

Lectures
--------

The lecture segments are available below and at this playlist:

`BF528 - Lecture 12 ChIP-Seq <https://www.youtube.com/playlist?list=PLpm6hAgFrR76a9BI2DIX-FpFtyf2kbg3C>`_

Introduction
++++++++++++

Playtime: 8:00

.. youtube:: Ekpce0pbkMY

Why ChIP-Seq?
+++++++++++++

Playtime: 4:44

.. youtube:: yh9crrAnYNE

Research Questions
++++++++++++++++++

Playtime: 4:27

.. youtube:: xdW5-1U-2jo

ChIP-Seq Analysis
+++++++++++++++++

Playtime: 8:14

.. youtube:: dmr9Tm8GK4Q

ChIP-Seq Experimental Design
++++++++++++++++++++++++++++

Playtime: 4:16

.. youtube:: VozZUMY6NYI

ChIP-Seq Peaks Analysis
+++++++++++++++++++++++

Playtime: 7:41

.. youtube:: Vsq3A2EPH-U

Workflow and QC
+++++++++++++++

Playtime: 5:01

.. youtube:: t4udwNPdhxo

Motifs
++++++

Playtime: 10:12

.. youtube:: zNLFC4SH7GI

What's My Protein Doing?
++++++++++++++++++++++++

Playtime: 2:15

.. youtube:: AjHSrZY02gM
