Databases
=========

This lecture covers an overview of publicly available databases that are
used to store biological data.

* :download:`pdf slides <databases.pdf>`
* :download:`pptx slides <databases.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTHx-LAMZbtve9yQ41aaLdayG9EQBf9pwWLCFzubAGzbh-h9QpWW6n60gVgvHInyyjkfW3pC6fVSIir/pub?start=false&loop=false&delayms=60000

Lectures
--------

Intro
+++++

Playtime: 10:34

.. youtube:: YpGhEPT9e8U

Database Types
++++++++++++++

Playtime: 5:52

.. youtube:: XmJIoa4XSU0

International Database Collaborations
+++++++++++++++++++++++++++++++++++++

Playtime: 2:17

.. youtube:: yIJxIMp7ML4

Medical Journal DBs
+++++++++++++++++++

Playtime: 4:26

.. youtube:: oosJCWYLxBE

Genomics and Transcriptomics
++++++++++++++++++++++++++++

Playtime: 6:55

.. youtube:: NYJWih3Bqn4

Proteomics
++++++++++

Playtime: 10:00

.. youtube:: jqZbkX356IHg

Refseq
++++++

Playtime: 6:06

.. youtube:: WIeMLq0ImWg

Other DBs
+++++++++

Playtime: 6:40

.. youtube:: UaSxgxxI1KI

BLAST
+++++

Playtime: 7:50

.. youtube:: Mrsv_XOYZWs

Assignment
----------

Project work
