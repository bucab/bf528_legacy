2nd Gen Sequencing Outline
==========================

#. Introduction
	#. Why we want to sequence?
	#. The experiment
	#. Advantages and differences
#. Machines
	#. Illumina
	#. 454
	#. Nanopore
	#. Ion Torrent
#. Comparison
	#. Error profile
	#. Throughput
	#. Cost
#. Definitions and standards
#. Statistics
#. Quality control
	#. FastQC
#. Online databases
	#. NCBI
	#. Illumina basespace
	#. Google genomics
	#. Amazon AWS
#. Other types of sequencing
	#. Pooled clone sequencing
	#. 10X genomics
	#. 3rd generation (long-read sequencing)
	#. Hi-C

