2nd Gen Sequencing
==================

This lecture covers an overview of second generation sequencing
data.

* :download:`pdf slides <2nd_generation_sequencing.pdf>`
* :download:`pptx slides <2nd_generation_sequencing.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTzyfENJGLok-iFfBGnQJsLkxgi85tiRYFPo1SMEs6QMRgdRuhlglLwxOz3MrRc_Ne26chdT3TY3d6X/pub?start=false&loop=false&delayms=60000

Lectures
--------

Introduction
++++++++++++

Playtime: 10:08

.. youtube:: ABoMQvxV16Q

Sequencing Design Choices
+++++++++++++++++++++++++

Playtime: 4:49

.. youtube:: XAFGwc3KCb8

Read Mapping
++++++++++++

Playtime: 4:42

.. youtube:: deuz6V_Vdg8

Mapping Statistics
++++++++++++++++++

Playtime: 7:46

.. youtube:: JNhRJ3ZHKmM

fastq Format
++++++++++++

Playtime: 6:03

.. youtube:: jL7vTa_v19s

Sequencing Platform Comparison
++++++++++++++++++++++++++++++

Playtime: 6:08

.. youtube:: nKENYSMMvzE

Sequencing Databases
++++++++++++++++++++

Playtime: 4:12

.. youtube:: UnRmD_LCRZg

Read Simlulation and Discussion
+++++++++++++++++++++++++++++++

Playtime: 15:10

.. youtube:: eTeRdC16x2Y

Assignment
----------

Project work
