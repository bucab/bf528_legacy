Microbiome: Metagenomics
========================

This lecture introduces the concept of the metagenome and how we can use
sequence information to learn about microbial ecologies.

* :download:`pdf slides <microbiome_metagenomics.pdf>`
* :download:`pptx slides <microbiome_metagenomics.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQnnx58Hgxn8jmE1lJizH3A_8NUCZ9_BOdUGHrr9GySVcyL5zJUXTsMorSb6j8PC8SQdnOEn5yl7KdC/pub?start=false&loop=false&delayms=60000

Lectures
--------

The same video lecture is for both Microbiome 16S and Metagenomic
lectures

`Microbiome`_

.. _Microbiome: https://mymedia.bu.edu/media/BF528+-+Microbiome+%2816S+%26+Metagenomic+Sequencing%29/1_qgd48kve
