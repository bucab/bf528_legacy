Genomic Variation and SNP Analysis Outline
==========================================

#. Introduction
	#. Genomic variant
	#. Classsification by size
	#. Classification by site
	#. Human and primates
#. Germline vs. somatic 
#. Consensus and profiles
	#. Conservation
#. Heterozygous vs homozygous
#. Discovery vs. genotyping
#. SNP and indel analysis
	#. Prevelance
	#. Strategies
	#. Tools
	#. Statistics
#. Structural variants
	#. Prevelance
	#. Mechanism 
		#. NAHR
		#. NEJH
	#. Read signatures
	#. Tools
	#. Challenges
#. GWAS
#. Validation methods
	#. Long reads
	#. FISH
	#. Array CGH
	#. SNP arrays
#. Visualization (link)
	
