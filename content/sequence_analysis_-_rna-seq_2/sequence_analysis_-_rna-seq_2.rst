Sequence Analysis - RNA-Seq 2
=============================

This lecture covers RNA-Seq analysis methods.

* :download:`pdf slides <sequence_analysis_-_RNA-Seq_2.pdf>`
* :download:`pptx slides <sequence_analysis_-_RNA-Seq_2.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTiGpkqsPN6GdcBsRrKeg-Z28W8b-r0BMKq74mHs6HVB4s5yyzhahddvxhtf7pRmkrzgypOyYhGeetC/pub?start=false&loop=false&delayms=60000
