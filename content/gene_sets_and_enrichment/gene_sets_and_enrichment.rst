Gene sets and enrichment
========================

This lecture describes the concept of gene sets and gene set enrichment
methods.

* :download:`pdf slides <Lecture_Gene_sets_and_enrichment.pdf>`
* :download:`pptx slides <Lecture_Gene_sets_and_enrichment.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vSbmjCME7ucFrniZ1QMpK8P2hirfWNcHvBkGuuQnWxq2vdLDDFlquqB4GcMFGnKT39HyX1PNzZejH_h/pub?start=false&loop=false&delayms=60000

Lectures
--------

Intro
+++++

Runtime: 9:08

.. youtube:: rEZsnhUcRdk

Types and Sources of Enrichment
+++++++++++++++++++++++++++++++

Runtime: 2:47

.. youtube:: gigHN1fFXdw

Statistics
++++++++++

Runtime: 2:41

.. youtube:: s_nh1NujLos

GO and David
++++++++++++

Runtime: 8:22

.. youtube:: ZZEkYMVTdgk

GSEA Concepts
+++++++++++++

Runtime: 8:54

.. youtube:: PwanwIGXxNk

GSEA Input
++++++++++

Runtime: 5:59

.. youtube:: 2_PsHJu45NY

GSEA Results
++++++++++++

Runtime: 4:42

.. youtube:: uc-I-bOMkNQ

BINGO and EnrichR
+++++++++++++++++

Runtime: 2:50

.. youtube:: Vd-e1En3N_8
