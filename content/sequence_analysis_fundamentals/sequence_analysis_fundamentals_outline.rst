Sequence Analysis 1 - WGS/WES Outline
=====================================

#. Introduction
	#. WGS vs WES
	#. The experiment
	#. Where to do?
	#. Denovo vs. reference based analysis
#. Alignment
	#. The reference
	#. Tools 
	#. Comparison
	#. Mappability (dark matter)
#. Preparing a reference genome
	#. Downloading
#. The pipeline
	#. Align
	#. Sort and index
	#. Marking/removing duplicates
	#. Realign around indels
#. Challenges
#. Assembly
	#. denovo vs. guided
#. Tools
	#. samtools
	#. bedtools
#. data formats (link)
	