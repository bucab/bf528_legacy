Sequence Analysis Fundamentals
==============================

This lecture covers fundamental concepts in short read sequencing analysis.

* :download:`pdf slides <Sequence_Analysis_Fundamentals.pdf>`
* :download:`pptx slides <Sequence_Analysis_Fundamentals.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQJMtfWQoTTCOFH18eA59m4RwHUS5Y44u0M2Mj9GF6uZ-7ysGL0jUjnAuQzlGpIq9r6Ml76JdRGQa9r/pub?start=false&loop=false&delayms=60000

Assignment
----------

Project work
