Introduction
============

This lecture covers overview information on the course and a description of
the class and project structure. Results from the survey are also presented
and discussed. The lecture ends with an overview of bioinformatics.

* :download:`pdf slides <introduction.pdf>`
* :download:`pptx slides <introduction.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vRCP0p5ARvNRpPo6KzjGdcTvT1SBVoz8W7M6see7n-ricraiwD-VgfLqb-I7OAHjTnMqhrY48iVrlSw/pub?start=false&loop=false&delayms=60000

Lectures
--------

This lecture is available below or at `this playlist`_.

.. _this playlist: https://www.youtube.com/playlist?list=PLpm6hAgFrR75Y4bgYlM_vznOJ-9lhKIK-

Class Introduction
++++++++++++++++++

Playtime: 10:36

.. youtube:: 4PAWxJxjp54

Schedule
++++++++

Playtime: 6:26

.. youtube:: rsK9spFKrWc

Group Project Description
+++++++++++++++++++++++++

Playtime: 11:39

.. youtube:: -gKsCxSe7zA

Translational Bioinformatics 1
++++++++++++++++++++++++++++++

Playtime: 10:19

.. youtube:: ICxD8-66vNc

Translational Bioinformatics 2
++++++++++++++++++++++++++++++

Playtime: 8:09

.. youtube:: z9lsXr2_3KI

For Next Time and Adam Hates Clicking
+++++++++++++++++++++++++++++++++++++

Playtime: 3:35

.. youtube:: aXKoFGsBqz0

SCC Demo and lolcow.img
+++++++++++++++++++++++

Playtime: 6:25

.. youtube:: RdgD1j74uA0

Survey Results
++++++++++++++

Playtime: 6:44

.. youtube:: 6bX9dRj3fVc


Assignment
----------

Familiarize yourself with the material on basic command line usage found here:

`Workshop 0. Basic Linux and Command Line Usage <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/00_cli_basics/00_cli_basics.html>`_

