Introduction Outline
====================

#. Course Overview

   #. Instructor Introductions
   #. Goals

      #. Survey current bioinformatics techniques in translational studies
      #. Give you hands-on experience working with bioinformatics tools and
         high-throughput biological data
      #. Help you get awesome at reading and understanding papers that use
         bioinformatics in translational studies

   #. Prerequisites

      #. Molecular and cell biology
      #. BF527, BE505/BE605, or equivalent
      #. Good-to-haves

         #. Basic statistics knowledge
         #. Programming/linux/cluster experience

   #. Course Organization

      #. http://bf528.readthedocs.io/en/latest/index.html
      #. Wednesday and Friday 2:30-4:15 SED 208
      #. *Flipped* paradigm - videos online with lecture content, interactive
         discussion in class
      #. Online content will be generally limited to ~1 hr/class
      #. Class periods split into two ~45 minute segments:

         #. First half: discussion of assigned online material
         #. Second half: project group meeting and discussion

      #. You have been assigned into groups of 4 for the whole semester
      #. There are 4 projects assigned over the semester
      #. There are no homeworks, no exams

   #. Projects

      #. You have been assigned into groups of 4 based on your experience
      #. Project groups are for the whole semester
      #. There are 4 projects:

         #. Microarray Based Tumor Classification
         #. Transcriptional Profile of Mamalian Cardiac Regeneration with
            mRNA-Seq
         #. Assessing The Effects of Chemical Treatments with RNA-Seq
         #. Choice of:

            #. Biomarker Discovery & Development
            #. A Microbiome Profile of a Family
            #. Frequent Mutations in Cancer

      #. Each project has a full writeup

   #. Project Groups

      #. Each person in each group will play one of four roles on each project:

         #. Data Curator - find, download, and organize data
         #. Programmer - process data into a form that can be analyzed
         #. Analyst - transform processed data into interpretable form
         #. Biologist - understand the paper and biological context, help
            interpret results

      #. The roles rotate for each project, so each person fulfills each role
         for one project
      #. In each class period, groups will have a "stand-up" session to discuss
         progress on their projects

   #. Assessment

      #. TODO

#. Grand Intro

   #. Biology as a Data Science - biological data timeline slide
   #. Big Data - data and database sizes slide
   #. What is bioinformatics?
   #. Venn Diagram slide
   #. Brief History of Bioinformatics slide
   #. Bioinformatics Topics and Examples slide
   #. Translational Bioinformatics
