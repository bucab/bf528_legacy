Sequence Visualization Outline
==============================

#. Source
	#. Griffith lab's tutorial on the Integrative Genomics Viewer (IGV)
	#. Link: https://github.com/griffithlab/rnaseq_tutorial/wiki/IGV-Tutorial#load-a-genome-and-some-data-tracks

#. Introduction
	#. Brief note about sequence visualization
		#. IGV and UCSC Genome Browser are probably the most popular
		#. UCSC is great for quick reference online
		#. I like IGV better for inspecting experiment quality and integrating information
		#. Both have similar functionality
	#. Goals and scope of the tutorial
		#. Quickly navigate around the genome
		#. Visualizing sequence alignments
		#. Get a sense for what structural features look like by eye
			#. Some real world examples of what to look for in translational bioinformatics
	#. Requirements
	#. Compatibility
	#. Data sets used in tutorial
	
#. Getting familiar with IGV
	#. Loading genomes and data tracks
	#. Navigating the genome
	#. Regions of interest
	#. Loading read alignments
	#. Visualizing read alignments
		#. Toggling how read alignments are sorted, grouped, and colored
		#. Toggling how read information is shown
		#. Viewing read information for a single aligned read
		
#. Using IGV to inspect your sequencing data for biological phenomena
	#. Two neighboring SNPs example
	#. Homopolymer region with indel example
	#. Coverage by GC content
	#. Heterozygous SNPs on different alleles
	#. Low mapping quality
	#. Homozygous deletions
	#. Mis-alignment
	#. Translocation

#. Automating tasks in IGV
	#. Running batch scripts in IGV
	#. Screenshot example

#. Extension beyond Griffith lab tutorial
	#. Generating publication quality images in IGV
	