Array Technologies Outline
==========================

#. What is a microarray?

   #. Characteristics of genes within a cell

      #. ~25k genes encoded in human genome
      #. genes have specific rols in the cell
      #. RNA abundance is termed *expression level* of the corresponding gene
      #. not all genes are active in all contexts, comparing the activity level
         of genes between contexts is termed *differential expression*

   #. Research question: which genes are differentially expressed between
      contexts?
   #. Microarray basics

      #. DNA and RNA with complementary sequences tend to hybridize together
         while non-specific strands do not
      #. If we know the nucleotide sequence of a RNA or DNA molecule, we can
         design complementary cDNA that will hybridize to target
      #. Microarrays are tiny platform chips that have complementary DNA
         sequences, termed *probes*, for specific gene sequences attached to
         their surface

   #. Microarray applications

      #. Experimental questions: e.g. disease vs normal, with and without
         treatment
      #. Data driven questions: e.g. which genes are differentially expressed?
         do some genes' expression predict a phenotype?

   #. Impact of microarrays

#. Spotted (two-color) microarrays

   #. Design

      #. Probe oligonucleotide sequences obtained from GenBank or dbEST
      #. Probes synthesized and amplified using PCR, then "spotted" onto
         the microarray slide
      #. Sequences usually range from 20-100nt
      #. Custom arrays can be created in individual labs with relatively
         affordable equipment

   #. Spotted array protocol

      #. Isolate RNA/cDNA of interest from two conditions
      #. tag sample with fluorescently labeled molecules of different
         wavelengths
      #. hybridize both tagged samples to a single array
      #. wash the array

   #. Spotted array processing

      #. prepared array is put on a scanner
      #. excitation wavelengths applied
      #. image taken
      #. each spot is a color corresponding to a blend of the two colors
      #. ratios are calculated from color, indicating relative fold change
         of abundance, i.e. "how much green" vs "how much red"
      #. many data corrections needed
      #. spotted arrays are long obsolete

#. Oligonucleotide arrays

   #. Design

      #. Commonly called Affymetrix (or affy) arrays, or simply *microarrays*
      #. Probes synthesized *in situ* (i.e. on the chip) using photolithography
      #. Probes are chemically limited to 25 bases (called an *oligo* or *25mer*)
      #. Terminology:

         probe
            an individual 25mer
         probe cell
            millions of copies of the same probe placed together in groups
         probe set
            set of unique 25mers corresponding to a particular gene

   #. Microarray synthesis cartoon
   #. Microarray probes

      #. Probes are specifically designed to represent a single location in the
         genome/transcriptome
      #. Each gene is represented by a unique set of 10-25 probes
      #. In older chips, each probe has a corresponding *mismatch probe* that
         has the same sequence as the real probe with a single different
         nucleotide, used to eliminate nonspecific binding effects
      #. Most modern microarrays do not have a mismatch probe

   #. Microarray processing

      #. Sample cDNA is tagged with a fluorescent marker and applied to the
         array
      #. cDNA fragments hybridize to complementary probes
      #. Unbound cDNA washed away
      #. Chip is put in scanner for fluorescent activation and scanning
      #. Intensity of probe fluorescence is proportional to the copy number of
         complementary RNA/DNA fragments in sample

   #. Common microarray platforms

      #. Earliest human oligo arrays were Affymetrix U133, contained mismatch
         probes, designed against 2000 draft human genome
      #. Replaced by Gene ST arrays, more genes, no mismatch probes
      #. Many types of arrays:

         #. Human, Mouse, Rat, and more expression arrays
         #. Human whole transcriptome (mRNA isoforms)
         #. ChIP-Chip - regulatory DNA regions after chromatin
            immunoprecipitation experiments
         #. miRNA chip
         #. SNP arrays - common human SNPs for genotyping

#. Microarray QC + Analysis

   #. Typical Processing Steps

      #. Samples collected and applied to microarray
      #. Each array generates a data file (.CEL file) containing quantified
         intensities for a panel of genes
      #. Normalize Arrays (make arrays comparable), quality control (eliminate
         bad samples, remove batch effects)
      #. Data Analysis (e.g. differential expression)

   #. Normalization

      #. Preprocessing steps that are employed before analysis can be performed
      #. Three types:

         #. Background correction - remove local artifacts and noise
         #. Data normalization - remove array-specific effects
         #. Probe summarization - combine probe intensities for probes that map
            to the same gene

      #. Old strategy: MAS5

         #. Measured Value = Noise + Probe Effects + Signal
         #. Background correction
         #. Intensity adjustment ses both perfect match and mismatch probes
         #. Obsolete in favor of RMA

      #. Current strategy: RMA

         #. Robust Multi-array Average
         #. Background correction using kernel density estimation
         #. Normalization by quantile to ensure all arrays have the same
            overall intensity distribution
         #. Summarization via linear additive model that protects against
            outlier probes

   #. Quality Control Metrics and Techniques

      #. RNA Integrity Number (RIN) - measure of RNA quality (Agilent tech.)
      #. Relative Log Expression (RLE) - subtract median gene expression across
         all arrays from each array
      #. Normalized Unscaled Standard Error (NUSE) - measure of precision of an
         array's expression estimate compared to all other arrays in batch
      #. Principal Component Analysis (PCA) - summarize data by vectors
         explaining greatest variance

   #. Quality Control Rules of Thumb

      #. Consider all of:

         #. median RLE > 0.1
         #. median NUSE > 1.0
         #. RIN < 4
         #. PCA to visually identify potential outliers

      #. No one metric or technique is necessarily grounds for elimination of
         a sample
      #. Failure of multiple thresholds may warrant closer examination

   #. Batch Effects

      #. Batch effects are trends in data that are a function of technical
         artifact, data generation, or poor experimental design
      #. Can confound comparison of interest
      #. ComBat - batch correction method implemented in R

   #. Batch Effect PCA example
