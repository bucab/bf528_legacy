Array Technologies
==================

This lecture covers an overview of microarrays and analysis.

* :download:`pdf slides <microarrays.pdf>`
* :download:`pptx slides <microarrays.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQ5tOnr9uT3vOu-1Idrc_5FZGvG80vrBggdFQTn_G76zkZAl-LY38HexjCBJR-1BHZqybRNRaWH9aHw/pub?start=false&loop=false&delayms=60000

Assignment
----------

Familiarize yourself with the Workshop 4 on R, up through the Data Wrangling section (and keep going if you want to):

`Workshop 4. Introduction to R <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/04_R/04_R.html>`_
