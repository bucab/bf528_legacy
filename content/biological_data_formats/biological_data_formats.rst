Biological Data Formats
=======================

This lecture covers an overview of standardized data formats
used to store biological data.

* :download:`pdf slides <Biological_Data_Formats.pdf>`
* :download:`pptx slides <Biological_Data_Formats.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTAlHBQ0ciGwBMgDAOLijFpez83CWB89joeVXGCh41M_UYa00I8Ic-rF8paVdGyrk39Mg5je7OL1fNH/pub?start=false&loop=false&delayms=60000

Lectures
--------

Intro, fasta, fastq
+++++++++++++++++++

Playtime: 5:07

.. youtube:: KX6KZQlk2jE

SAM, BAM, CRAM
++++++++++++++

Playtime: 6:51

.. youtube:: Bpf_KVsU_D8

Tabular Data, awk
+++++++++++++++++

Playtime: 8:37

.. youtube:: nhXJ0scdUL4

Genomic Feature Formats
+++++++++++++++++++++++

Playtime: 4:01

.. youtube:: HR3t7YCpOt4

BED Format
++++++++++

Playtime: 6:57

.. youtube:: kgul8N9RAkc

GFF and GTF
+++++++++++

Playtime: 3:38

.. youtube:: 552Zvt9aSRw

Format Summary
++++++++++++++

Playtime: 5:13

.. youtube:: fcOFWwIVUkg

Examples
++++++++

Playtime: 3:09

.. youtube:: ewwU2arw98s

UCSC Genome Browser and Tools
+++++++++++++++++++++++++++++

Playtime: 8:50

.. youtube:: VnauYiYwrDE


Assignment
----------

Project work
