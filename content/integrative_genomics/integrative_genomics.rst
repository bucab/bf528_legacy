Integrative Genomics
====================

This lecture discusses concepts in integrative genomics.

* :download:`pdf slides <integrative_genomics.pdf>`
* :download:`pptx slides <integrative_genomics.pptx>`
* `online slides`_

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vRmNM78YD0Ah8DHuwBwH4JmndxT8MeljqSQvknkyw-p61MNJs7UOt69FbIcu69nFwtTLzEio86WBj-t/pub?start=false&loop=false&delayms=60000

Lectures
--------

`Integrative Genomics`_

.. _Integrative Genomics: https://mymedia.bu.edu/media/BF528+-+Integrative+Genomics/1_6wv42fma
