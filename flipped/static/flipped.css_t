/* vim: set syntax=css ts=4 expandtab shiftwidth=4 list: */
/*
 * haiku.css_t
 * ~~~~~~~~~~~
 *
 * Sphinx stylesheet -- haiku theme.
 *
 * Adapted from http://haiku-os.org/docs/Haiku-doc.css.
 * Original copyright message:
 *
 *     Copyright 2008-2009, Haiku. All rights reserved.
 *     Distributed under the terms of the MIT License.
 *
 *     Authors:
 *              Francois Revol <revol@free.fr>
 *              Stephan Assmus <superstippi@gmx.de>
 *              Braden Ewing <brewin@gmail.com>
 *              Humdinger <humdingerb@gmail.com>
 *
 * :copyright: Copyright 2007-2017 by the Sphinx team, see AUTHORS.
 * :license: BSD, see LICENSE for details.
 *
 */

@import url("basic.css");
@import url('https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Roboto+Slabo');

{% set dark = 'rgba(104, 0, 24, 1); /* dark */' -%}
{% set medium = 'rgba(204, 0, 0, 1); /* medium */' -%}
{% set medlight = 'rgba(226, 24, 38, 1); /* medlight */' -%}
{% set light = 'rgba(224, 78, 42, 1); /* light */' -%}
{% set lightest = 'rgba(239, 181, 55, 1); /* lightest */' -%}

html {
    margin: 0px;
    padding: 0px;
    background: #fafafa; /* url(bg-page.png) top left repeat-x; */
}

body {
    line-height: 1.5;
    margin: auto;
    padding: 3px;
    font-family: 'Open Sans', sans-serif;
    min-width: 43em;
    max-width: 80em;
    color: #333333;
}
div.body {
    margin-left: auto;
    margin-right: auto;
}
div.footer {
    padding: 8px;
    font-size: 11px;
    text-align: center;
    letter-spacing: 0.5px;
}
div.contents {
    margin-left: 5px;
    margin-right: 5px;
}

/* link colors and text decoration */

a:link {
    font-weight: bold;
    text-decoration: none;
    /*color: #dc3c01;*/
    color: {{ medium }};
}

a:visited {
    font-weight: bold;
    text-decoration: none;
    color: {{ medlight }};
    /*color: #892601;*/
}

a:hover, a:active {
    text-decoration: underline;
    /*color: #ff4500;*/
    color: {{ light }};
}

/* Some headers act as anchors, don't give them a hover effect */
h1, h2, h3, h4 {
    font-family: 'Roboto', sans-serif;
    padding-left: 5px;
    padding-right: 5px;
    background: #efefef;
    border-width: 0px 1px 1px 0px;
    border-style: solid;
    border-color: {{ dark }};
    color: {{ dark }};
    margin-top: 30px;
}
h1 {
    font-size: 2.3em;
    font-weight: bold;
    color: {{ dark }};
    /* border-bottom: dotted thin {{ medium }}; */
}

h2 {
    font-size: 1.2em;
    font-weight: normal;
    color: {{ dark }};
    /* border-bottom: dotted thin #e0e0e0; */
    margin-top: 30px;
}

h3 {
    font-size: 1.1em;
    font-weight: normal;
    color: {{ dark }};
    margin-top: 30px;
}

h4 {
    font-size: 1.0em;
    font-weight: normal;
    color: {{ dark }};
    margin-top: 30px;
}
h1 a:hover, a:active {
    text-decoration: none;
    color: {{ dark }};
}

h2 a:hover, a:active {
    text-decoration: none;
    color: {{ dark }};
}

h3 a:hover, a:active {
    text-decoration: none;
    color: {{ dark }};
}

h4 a:hover, a:active {
    text-decoration: none;
    color: {{ dark }};
}

a.headerlink {
    padding-left: 5px;
    color: {{ medium }};
}

a.headerlink:hover {
    color: #a7ce38;
}

/* basic text elements */

div.content {
    margin-top: 20px;
    margin-left: 40px;
    margin-right: 40px;
    margin-bottom: 50px;
    font-size: 0.9em;
}

div.section {
    padding-left: 3px;
    padding-right: 3px;
}
/* heading and navigation */

div.header {
    position: relative;
    left: 0px;
    top: 0px;
    height: 85px;
    /* background: #eeeeee; */
    padding: 0 40px;
}
div.header h1 {
    font-size: 2.6em;
    font-weight: normal;
    letter-spacing: 1px;
    color: {{ dark }};
    border: 0;
    margin: 0;
    padding-top: 15px;
}
div.header h1 a {
    font-weight: normal;
    color: {{ dark }};
}
div.header h2 {
    font-size: 1.3em;
    font-weight: normal;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: #aaa;
    border: 0;
    margin-top: -3px;
    padding: 0;
}

div.header img.rightlogo {
    float: right;
}


div.title {
    font-size: 1.3em;
    font-weight: bold;
    color: {{ dark }};
    border-bottom: 5px solid {{ medium }};
    margin-bottom: 25px;
}
div.topnav {
    /* background: #e0e0e0; */
}
div.topnav p {
    margin-top: 0;
    margin-left: 40px;
    margin-right: 40px;
    margin-bottom: 0px;
    text-align: right;
    font-size: 0.8em;
}
div.bottomnav {
    background: #eeeeee;
}
div.bottomnav p {
    margin-right: 40px;
    text-align: right;
    font-size: 0.8em;
}

a.uplink {
    font-weight: normal;
}


/* contents box */

table.index {
    margin: 0px 0px 30px 30px;
    padding: 1px;
    border-width: 1px;
    border-style: dotted;
    border-color: #e0e0e0;
}
table.index tr.heading {
    background-color: #e0e0e0;
    text-align: center;
    font-weight: bold;
    font-size: 1.1em;
}
table.index tr.index {
    background-color: #eeeeee;
}
table.index td {
    padding: 5px 20px;
}

table.index a:link, table.index a:visited {
    font-weight: normal;
    text-decoration: none;
    color: #dc3c01;
}
table.index a:hover, table.index a:active {
    text-decoration: underline;
    color: #ff4500;
}

table.docutils {
    border: 1px solid #aaa;
    width: 1px;
    white-space: nowrap;
    margin-left: 15px;
    margin-right: auto;
}

tr:nth-child(even) {
    background: #ccc;
}

tr:nth-child(even) {
    background: #eee;
}

/* Haiku User Guide styles and layout */

/* Rounded corner boxes */
/* Common declarations */
div.admonition {
    -webkit-border-radius: 10px;
    -khtml-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    border-style: dotted;
    border-width: thin;
    border-color: #dcdcdc;
    padding: 10px 15px 10px 15px;
    margin-bottom: 15px;
    margin-top: 15px;
}
div.note {
    padding: 10px 15px 10px 80px;
    background: #e4ffde url(alert_info_32.png) 15px 15px no-repeat;
    min-height: 42px;
}
div.warning {
    padding: 10px 15px 10px 80px;
    background: #fffbc6 url(alert_warning_32.png) 15px 15px no-repeat;
    min-height: 42px;
}
div.seealso {
    background: #e4ffde;
}

/* More layout and styles */
p {
    text-align: justify;
    padding-left: 5px;
    padding-right: 5px;
}

p.last {
    margin-bottom: 0;
}

ol {
    padding-left: 20px;
}

ul {
    padding-left: 30px;
    margin-top: 3px;
}

li {
    line-height: 1.3;
}

div.content ul > li {
    -moz-background-clip:border;
    -moz-background-inline-policy:continuous;
    -moz-background-origin:padding;
    background: transparent url(bullet_medlight.png) no-repeat scroll left 0.45em;
    list-style-image: none;
    list-style-type: none;
    padding: 0 0 0 1.666em;
    margin-bottom: 3px;
}

td {
    vertical-align: top;
}

code {
    background-color: #e2e2e2;
    font-size: 1.0em;
    font-family: monospace;
}

pre {
    border-color: {{ dark }};
    border-style: dotted;
    border-width: thin;
    margin: 0 0 12px 0;
    padding: 0.8em;
    background-color: #f0f0f0;
}

hr {
    height: 2px;
    border-top: 2px solid {{ medium }};
    border-bottom: 0;
    border-right: 0;
    border-left: 0;
    margin-bottom: 10px;
    margin-top: 20px;
}

/* printer only pretty stuff */
@media print {
    .noprint {
        display: none;
    }
    /* for acronyms we want their definitions inlined at print time */
    acronym[title]:after {
        font-size: small;
        content: " (" attr(title) ")";
        font-style: italic;
    }
    /* and not have mozilla dotted underline */
    acronym {
        border: none;
    }
    div.topnav, div.bottomnav, div.header, table.index {
        display: none;
    }
    div.content {
        margin: 0px;
        padding: 0px;
    }
    html {
        background: #FFF;
    }
}

.viewcode-back {
    font-family: "DejaVu Sans", Arial, Helvetica, sans-serif;
}

div.viewcode-block:target {
    background-color: #f4debf;
    border-top: 1px solid #ac9;
    border-bottom: 1px solid #ac9;
    margin: -1px -10px;
    padding: 0 12px;
}

/* math display */
div.math p {
    text-align: center;
}
