conda install -c conda-forge -c bioconda sra-tools bowtie2 samtools=1.4.0 subread salmon=1.1.0
pip install -r requirements.txt
