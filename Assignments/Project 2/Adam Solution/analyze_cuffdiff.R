d <- read.table('gene_exp.diff',header=T,as.is=T)
hist(d$log2.fold_change.,breaks=100)
dev.copy(png,'all_cufflinks_fc.png')
dev.off()

d.sig <- subset(d,significant=="yes")
hist(d.sig$log2.fold_change.,breaks=100)
dev.copy(png,'sig_cufflinks_fc.png')
dev.off()

d.sig <- d.sig[order(d.sig$q_value),]
d.sig.up <- subset(d.sig,log2.fold_change.>0)
d.sig.dn <- subset(d.sig,log2.fold_change.<0)

write(d.sig.up$gene,"up_genes.txt")
write(d.sig.dn$gene,"dn_genes.txt")
