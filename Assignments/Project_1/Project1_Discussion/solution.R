#Solution Project 1 - Ana

##Part 3, point 2
source("http://bioconductor.org/biocLite.R")

shared.library.path <- file.path("/unprotected/projects/cbmhive/R_packages", sprintf("R-%s", getRversion()));
.libPaths(c(shared.library.path, .libPaths()));

library(affy)

##Part 3, point 4
ann <- read.table('filenames.txt', header=F, check.names=F)
all.data <- ReadAffy(filenames=as.character(ann$V1), verbose=T)

saveRDS(all.data,'all.data.rds')

all.data <- readRDS('all.data.rds')

## pkg <- cleancdfname(cdfName(all.data))
## ok <- require(pkg, character.only=TRUE)
## if (!ok)
##    stop(pkg, " not found; install using biocLite?")
##biocLite("hgu133plus2cdf")
   
all.data.rma <- rma(all.data)
saveRDS(all.data.rma,'all.data.rma.rds')

## Part 3, point 5 http://www.biomedcentral.com/1471-2105/12/137
library(affyPLM)
all.data <- readRDS('all.data.rds')
pset <- fitPLM(all.data) ##Loading required package: AnnotationDbi takes really long. Also fitPLM requires AffyBatch, not ExpressionSet

pdf('rle.pdf')
RLE(pset,main="RLE")
dev.off()

pdf('nuse.pdf')
NUSE(pset,main="NUSE")
dev.off()

## Part 3, point 6
library(sva)
pheno <- read.csv('proj_metadata.csv')
all.data.rma <- readRDS('all.data.rma.rds')
 
exprs.all.data.rma <-exprs(all.data.rma)
colnames(exprs.all.data.rma) <- gsub('_.*','',colnames(exprs.all.data.rma))
rownames(pheno) <- as.character(pheno$geo_accession)
pheno <- pheno[intersect(colnames(exprs.all.data.rma),rownames(pheno)),]
 
exprs.all.data.rma <- exprs.all.data.rma[,intersect(colnames(exprs.all.data.rma),rownames(pheno))]
combat.exprs <- ComBat(exprs.all.data.rma,pheno$normalizationcombatbatch,pheno$normalizationcombatmod)

## Part 3, point 7
m <- t(scale(t(combat.exprs)))
data.pca <- prcomp(m, center=FALSE)
pdf('pca.pdf')
plot(data.pca$rotation[,1] ,data.pca$rotation[,2], xlab="PC 1", ylab="PC 2", main="PCA_by_sample") #plot
dev.off()

## Part 4, point 1
combat.exprs.filter <- combat.exprs[rowSums(combat.exprs > log2(15)) > 5/100 * ncol(combat.exprs), ]
nrow(combat.exprs.filter) # 42618

## Part 4, point 2
# http://www.itl.nist.gov/div898/handbook/eda/section3/eda358.htm

combat.exprs.filter.var <- apply(combat.exprs.filter, 1, var) # compute variance for each gene
combat.exprs.filter.var.median <- median(combat.exprs.filter.var) # find median variance

alpha = 0.01 # significance level
degf = ncol(combat.exprs.filter) - 1 # degree of freedom
chi.test.stat = degf * combat.exprs.filter.var / combat.exprs.filter.var.median # t-statistic for chi-square test
chi.test = chi.test.stat > qchisq(1 - alpha, degf) # actual test

combat.exprs.filter <- combat.exprs.filter[chi.test, ] # filter genes
nrow(combat.exprs.filter) # 16791

## Part 4, point 3
coeff.var <- apply(combat.exprs.filter, 1, sd) / rowMeans(combat.exprs.filter)
combat.exprs.filter <- combat.exprs.filter[coeff.var > 0.186, ]
nrow(combat.exprs.filter) # 1735



## Part 5, point 1
tree <- hclust(dist(t(combat.exprs.filter)))
## Part 5, point 2
K = 2
clusters <- cutree(tree, k=K)

# heatmap -- not in exercise
pdf('exprssion_clusters_heatmap.pdf')
#plot(tree)
labels <- ifelse(pheno$cit.coloncancermolecularsubtype=="C3","red","blue")
heatmap(combat.exprs.filter, ColSideColors=labels)
dev.off()

## Part 5, point 3
cl = 1
cluster.test <- function(x) {
    return(t.test(x[clusters == cl], x[clusters  != cl])$p.value)
}

genes.cluster.test.diff <- apply(combat.exprs.filter, 1, cluster.test)
rownames(combat.exprs.filter)[order(genes.cluster.test.diff)]

## Part 5, point 4
rownames(combat.exprs.filter)[genes.cluster.test.diff < 0.05] # differentially expressed
sum(genes.cluster.test.diff < 0.05) # number of differentially expressed - 1381

