from collections import OrderedDict
import pandas as pd
from glob import glob

with open('accession_sample_map.txt_tmp') as f :
  d = OrderedDict()
  for r in f :
    fname, rest = r.split('\t',1)
    fname = fname.replace('!Sample_','')
    rest = [v.replace('"','').strip() for v in rest.split('\t')]
    if fname == 'characteristics_ch1' :
      fname, val = rest[0].split(':',1)
      rest = [v.replace('%s: '%fname,'') for v in rest]
    d[fname] = rest

map_df = pd.DataFrame(zip(*d.values()),columns=d.keys())
map_df.index = map_df.title

meta_df = pd.read_csv('journal.pmed.1001453.s009.csv',index_col=0)

all_df = meta_df.join(map_df,rsuffix='_acc')
proj_samps = (all_df.SixSubtypesClassification=='C4') | (all_df.SixSubtypesClassification=='C3')

proj_df = all_df[proj_samps]

cel_dir = '/restricted/projectnb/mlhd/labadorf/bfXXX/project_1/CEL_files'
proj_df['CEL_path'] = [glob('%s/%s*'%(cel_dir,a))[0] for a in proj_df.geo_accession]

proj_df.to_csv('proj_metadata.csv')
