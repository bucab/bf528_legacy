#!/bin/bash -l

#$ -cwd
#$ -P bf528

module load R/3.2.3

Rscript process.R
Rscript qa.R
Rscript filter.R
Rscript analyze.R
