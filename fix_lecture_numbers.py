import os

old = [_.strip() for _ in open('old_order.txt').readlines()]
new = [_.strip() for _ in open('new_order.txt').readlines()]

for old_path, new_path in zip(old,new) :

  if os.path.exists(new_path) : os.system('rm -rf {}'.format(new_path))

  # rename files
  old_patt = old_path.replace('content/','')
  new_patt = new_path.replace('content/','')

  if old_patt == new_patt : continue

  # move the dir
  os.system("cp -r {} {}".format(old_path,new_path))

  for fn in os.listdir(old_path):
    fn = os.path.split(fn)[1]
    os.system("mv {}/{} {}/{}".format(
      new_path,fn,
      new_path,fn.replace(old_patt,new_patt)
    ))
    os.system("git rm {}/{}".format(old_path,fn))

  os.system("git rm -r {}/*".format(old_path))
  os.system("git add {}".format(new_path))
