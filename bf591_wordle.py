words = {'genetics': 10,
	 'genomics': 10,
	 'bioinformatics': 15,
	 'translational': 15,
	 'GWA': 5,
	 'pharmacogenomics': 5,
	 'biomarkers': 8,
	 'microbiome': 8,
	 'sequencing': 9,
	 'microarray': 9,
	 'RNA-Seq': 5,
	 'computation': 6,
	 'systems biology': 4,
	 'biological networks': 4,
	 'statistics': 4,
	 '?': 30,
	 'reproducibile research': 5
         }

print ' '.join(' '.join([w]*v) for w,v in words.items())
