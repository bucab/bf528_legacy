.. BF528 - Applications in Translational Bioinformatics documentation master file, created by
   sphinx-quickstart on Wed Dec 27 13:31:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BF528 - Applications in Translational Bioinformatics
====================================================

Welcome to the homepage of BF528.

**Meeting time:** Wednesday/Friday 2:30-4:15

**Location:** `FLR 123`_

**Office hours:**
Office hours TBD

.. _FLR 123: https://maps.bu.edu/?id=647#!m/400401

.. contents:: Table of Contents
   :local:
   :backlinks: top

Description
-----------

The objective of this course is expose students to the topics and technologies
used in modern bioinformatics studies. The course covers a mix of biological
and computational topics, including:

* High throughput genomics techniques (microarrays, 2nd generation sequencing)
* Current high throughput sequencing assays (DNA-Seq, RNA-Seq, ChIP-Seq)
* Differential gene expression techniques
* Microbiome/metagenomics techniques
* Metabolomics
* Proteomics
* Systems, network, and integrative biology
* Basic linux cluster usage
* Python and R scripting
* Computational workflow and replication strategies
* Genomics data visualization techniques
* Biological databases

This is highly hands-on course, where a portion of in-class periods are
dedicated to concerted group work and interactive discussions. The course
materials are focused on real-world applications of the high throughput
genomics techniques and organized into structured **group projects**. Students
are organized into groups of three or four for the entire semester, where they
will work together to replicate results from published studies. The tasks for
each project have been divided into four ‘roles’:

**Data curator**: identify, download, and describe relevant datasets and
literature
   
**Programmer**: write code to transform the downloaded data into an
interpretable form
   
**Analyst**: examine and visualize processed data to aid in interpretation
   
**Biologist**: connect the processed data into meaningful biological
interpretation

Each group will complete four projects over the course of the semester, and for
each project the roles of the members rotate, such that each member fulfills
each role once. Each group will produce a full report describing their work on
each project, including biological background, methods, results, and
interpretation.

The first half of each in-class period will be dedicated to interactive
discussions on special topics germane to the project material. The second half
will be devoted to “standup” sessions discussing the projects, sharing ideas,
and communicating challenges both within and between groups, with the
assistance of the instructors.

There are no homeworks or exams, only projects.

Prerequisites
-------------

Basic understanding of biology and genomics. Any of these courses are adequate
prerequisites for this course: BF527, BE505/BE605. Students should have some
experience programming in a modern programming language (R, python, C, Java, etc).

Instructor
----------

Adam Labadorf

Project Overview
----------------

.. toctree::
  :hidden:
  :glob:

  content/projects/*/*
  content/projects/project_reports

The four projects are as follows:

1. :doc:`content/projects/project_1_microarrays/project_1_microarrays`
2. :doc:`content/projects/project_2_rnaseq_1/project_2_rnaseq_1`
3. :doc:`content/projects/project_3_rnaseq_2/project_3_rnaseq_2`
4. :doc:`content/projects/project_4_scrnaseq/project_4_scrnaseq`
5. :doc:`content/projects/individual_project`

Project guidelines for group reports (1, 2, 3, and 4) are here:
:doc:`content/projects/project_reports`

Grading and Assessment
----------------------

Each project constitutes 10% report assessment, 10% individual assessment, and
5% standup participation of your overall grade. Report assessment will be the
same for all members; individual assessment will be based on role requirements.
Assessment is made based on the process completed by the group, not whether the
study results were successfully replicated or not.

Schedule
--------

**NB: Subject to change**

.. csv-table::
   :header: "Class", "Day", "Date", "Topic", "Project Out/Due", "Lecturer"
   :file: schedule_fmt.csv
   :align: center

Site Index
----------

.. toctree::
   :maxdepth: 2
   :glob:
   
   content/*/*


