# BF528

The objective of this course is expose students to the topics and technologies
used in modern bioinformatics studies. The course covers a mix of biological
and computational topics, including:

* High throughput genomics techniques (microarrays, 2nd generation sequencing)
* Current high throughput sequencing assays (DNA-Seq, RNA-Seq, ChIP-Seq)
* Differential gene expression techniques
* Phylogenetic techniques
* Microbiome/metagenomics techniques
* Metabolomics
* Proteomics
* Systems, network, and integrative biology
* Basic linux cluster usage
* Python and R scripting
* Computational workflow and replication strategies
* Genomics data visualization techniques
* Biological databases

## Environment Setup

You will need to set up your environment to use certain software for this
course. The following code needs to be run *only once*:

```
$ bash /projectnb/bf528/setup.sh
```

This will add the necessary files and code to your home directory to
configure your environment as needed.
