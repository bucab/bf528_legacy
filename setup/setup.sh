# add content to the user's .bashrc file to configure
if [ -z "$BF528HOME" ]; then
  echo "adding BF528HOME"
  cat >> ~/.bashrc <<EOF

# this was added for bf528 on `date +%F`
export BF528HOME=/projectnb/bf528
module load anaconda3
EOF
fi

source ~/.bashrc

# add content to condarc so we can make software
# available to users via conda
if [ ! -f ~/.condarc ]; then
  echo "creating ~/.condarc"
  cat > ~/.condarc <<EOF
# this was added for bf528 on `date +%F`
envs_dirs:
  - $BF528HOME/conda_root
EOF
else
  # .condarc exists and already has path, do nothing
  grep -q "$BF528HOME/conda_root" ~/.condarc
  if [ $? -eq 0 ]; then
    echo "your conda environment is already configured"
  else

    # .condarc exists and does not contain path, modify it

    # existing envs_dirs section
    grep -q envs_dirs ~/.condarc
    if [ $? -eq 0 ]; then
      echo "adding bf528 conda_root to existing envs_dirs in ~/.condarc"
      sed -i "/envs_dirs/ a \
\ \ - $BF528HOME/conda_root # this was added for bf528 on `date +%F` " ~/.condarc
    # no existing envs_dirs section
    else
      echo "adding bf528 conda_root to ~/.condarc"
      cat >> ~/.condarc <<EOF

# this was added for bf528 on `date +%F`
envs_dirs:
  - $BF528HOME/conda_root
EOF
    fi
  fi
fi
