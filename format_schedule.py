'''
This script takes the contents of schedule.csv and produces a new file 
schedule_fmt.csv file with Topic contents formatted as ReST links.

It also creates a schedule_toc.rst file that contains the toctree for the
appropriate files

It also creates the skeleton of the course under the content/ directory,
creating .rst files for each topic if they don't already exist.
'''
import calendar
import csv
from datetime import datetime
import io
import os
from pathlib import Path

import urllib.request

url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSVpQJcQW_-HbMB1B87II15RSfO2xp0-0ytMKq8We8K-xcwi9gQK7PWUUQplmCdgH3Be2ktd1gWGtsS/pub?gid=0&single=true&output=csv'

doc_dir = 'content'
if not os.path.isdir('content') :
    os.mkdir(doc_dir)

# change this year appropriately for the semester so the day of week is right
year = 2020

field_names = 'Lecture','Date','Topic','Secondary','online','outline','in-class'
with open('schedule_fmt.csv','w') as out_f :
    response = urllib.request.urlopen(url)
    resp = io.StringIO(response.read().decode())
    out_f = csv.writer(out_f)
    for i,rec in enumerate(csv.DictReader(resp)) :
        num, date, topic, secondary, online, outline, inclass = [rec[_] for _ in field_names]

        dayname = ''
        if date != '' :
          date_obj = datetime.strptime('{} {}'.format(date,year),'%b %d %Y')
          dayname = calendar.day_abbr[date_obj.weekday()]

        # no num means no class
        if num == '' :
          out_f.writerow([
            num,
            dayname,
            date,
            topic,
            secondary,
            rec['Project']
          ])
          continue

        safe_topic = topic.lower().translate(str.maketrans(' :/,','____'))

        # create the content dir if it doesn't exist
        dir_base = '{:0>2d}_{}'.format(int(num),safe_topic)
        dirname = Path(doc_dir).joinpath(dir_base)
        dirname.mkdir(exist_ok=True,parents=True)
        print(dirname)

        # create the files if they don't exist
        rst_path = dirname.joinpath(safe_topic+'.rst')
        online_link_rst = ''
        if online == '1' :
            online_link_rst = ':doc:`online <{}>`'.format(
              rst_path.with_suffix('')
            )
            if not rst_path.exists() :
                with open(rst_path,'wt') as f :
                    f.write(topic+'\n'+'='*len(topic)+'\n')

        rst_outline_path = dirname.joinpath(safe_topic+'_outline.rst')
        outline_link_rst = ''
        if outline == '1' :
            outline_link_rst = ':doc:`outline <{}>`'.format(
                rst_outline_path.with_suffix('')
            )
            if not rst_outline_path.exists() :
                print('outline')
                with open(rst_outline_path,'wt') as f :
                    title = topic + ' Outline'
                    f.write(title+'\n'+'='*len(title)+'\n')

        rst_inclass_path = dirname.joinpath(safe_topic+'_inclass.rst')
        inclass_link_rst = ''
        if inclass == '1' :
            inclass_link_rst = ':doc:`in class <{}>`'.format(
                rst_inclass_path.with_suffix('')
            )
            if not rst_inclass_path.exists() :
                with open(rst_inclass_path,'wt') as f :
                    title = topic + ' In Class'
                    f.write(title+'\n'+'='*len(title)+'\n')

        # use the online, outline, and inclass vars to create the appropriate
        # table format
        links = (online_link_rst, outline_link_rst, inclass_link_rst)
        links = [str(_) for _ in links if str(_) != '']
        out_f.writerow([
            num,
            dayname,
            date,
            '{} {}'.format(
                topic,
                ' | '.join(links)
            ),
            rec['Project'],
            rec['Guest']
        ])
