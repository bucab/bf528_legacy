import calendar
import csv
from datetime import datetime
import io
import os
from pprint import pprint
import re
from subprocess import run
import sys
import urllib.request

doit = False
if len(sys.argv) > 1 and sys.argv[1] == 'DOIT' :
    doit = True
else :
    print('friendly note: this script is destructive, not doing anything unless you provide DOIT on the command line...')

url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSVpQJcQW_-HbMB1B87II15RSfO2xp0-0ytMKq8We8K-xcwi9gQK7PWUUQplmCdgH3Be2ktd1gWGtsS/pub?gid=0&single=true&output=csv'

doc_dir = 'content'
if not os.path.isdir('content') :
    os.mkdir(doc_dir)

def parse_topic(topic) :
    return topic.lower().translate(str.maketrans(' :/,','____'))

year = 2020

# figure out the order of the local content
local_content_dirs = os.listdir('content')
local_content_dirs.remove('projects')
local_content_dirs.remove('old_lectures')

patt = re.compile('(?P<num>[0-9]+)_(?P<name>.*)')

local_content = {}
for d in local_content_dirs :
    print(d)
    m = patt.match(d)
    num, topic = m.groups()

    topic = parse_topic(topic)
    local_content[topic] = int(num)

remote_content = {}
field_names = 'Lecture','Date','Topic','online','outline','in-class'
with open('schedule_fmt.csv','w') as out_f :
    response = urllib.request.urlopen(url)
    resp = io.StringIO(response.read().decode())
    out_f = csv.writer(out_f)
    for i,rec in enumerate(csv.DictReader(resp)) :
        num, date, topic, online, outline, inclass = [rec[_] for _ in field_names]

        dayname = ''
        if date != '' :
          date_obj = datetime.strptime('{} {}'.format(date,year),'%b %d %Y')
          dayname = calendar.day_abbr[date_obj.weekday()]

        # no num means no class
        if num == '' :
          out_f.writerow([
            num,
            dayname,
            date,
            topic,
            rec['Proj']
          ])
          continue

        remote_content[parse_topic(topic)] = int(num)

    print('local content:')
    pprint(local_content)
    print('remote content:')
    pprint(remote_content)

    # ensure keys are the same
    missing_remote = set(local_content.keys()).difference(set(remote_content.keys()))
    print('Missing remote keys:', missing_remote)
    assert missing_remote == set()

    missing_remote = set(remote_content.keys()).difference(set(local_content.keys()))
    print('Missing local keys:', missing_remote)
    assert missing_remote == set()

    missing_remote = set(local_content.values()).difference(set(remote_content.values()))
    print('Missing remote lecnums:',missing_remote)
    assert missing_remote == set()

    missing_local = set(remote_content.values()).difference(set(local_content.values()))
    print('Missing local lecnums:',missing_local)
    assert missing_local == set()

    lectures = list(sorted(local_content.keys()))

    for lecture in lectures :
        local, remote = local_content[lecture], remote_content[lecture]
        if local != remote :
            cmd = 'git mv content/{:02d}_{} content/{:02d}_{}'.format(
                local,lecture,
                remote,lecture
            )
            print(cmd)
            if doit :
                run(cmd,shell=True)
            else :
                print('not doing it')
